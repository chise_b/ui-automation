package ass1.features.search;

import ass1.steps.serenity.MenCategorySteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class SelectMenCategoryStory {

    /*
        This test goes on men category and check if that page has been reached.
        The start page is considered the landing page.
     */

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public MenCategorySteps user;

    @Test
    public void select_new_arrivals_menu() {
        user.is_the_home_page();
        user.select_men_menu();
        user.check_is_on_men_page();
    }
}
