package ass1.features.search;

import ass1.steps.serenity.AddToCartSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class AddToCartFirstNewProductStory {
     /*
        Add to cart the first new product.
        Select color and size and add to cart.
        Check if the product was added to cart successfully.
        The start page is considered the landing page.
     */

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public AddToCartSteps user;

    @Test
    public void select_new_arrivals_menu() {
        user.is_the_home_page();
        user.select_first_new_product();
        user.select_color();
        user.select_size();
        user.add_to_cart();
        user.check_product_added_successfully_to_cart();
    }
}
