package ass1.features.search;

import ass1.steps.serenity.EndUserSteps;
import ass1.steps.serenity.LanguageNotWorkingSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.junit.Test;

@RunWith(SerenityRunner.class)
public class ChangeLanguageStory {

     /*
        Localization is not working.

        Changes the selected language and after that the place holder of the search box is checked
        to be the same as the english one.
        The start page is considered the landing page.
     */

    private static final int OTHER_LANGUAGE_INDEX = 2;
    private static final String TRANSLATION = "Search entire store here...";

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public LanguageNotWorkingSteps user;

    @Test
    public void checking_localization_is_not_working() {
        user.is_the_home_page();
        user.check_localization_not_working(OTHER_LANGUAGE_INDEX, TRANSLATION);
    }

}
