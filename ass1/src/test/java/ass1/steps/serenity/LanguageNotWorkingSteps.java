package ass1.steps.serenity;

import ass1.pages.LandingPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class LanguageNotWorkingSteps {
    LandingPage landingPage;

    @Step
    public void changes_language(int index) {
        landingPage.change_language(index);
    }

    @Step
    public void localization_is_not_working(String translation) {
        Assert.assertEquals(landingPage.getSearchBoxPlaceHolder(), translation);
    }

    @Step
    public void is_the_home_page() {
        landingPage.open();
    }

    @Step
    public void check_localization_not_working(int languageIndex, String translation) {
        changes_language(languageIndex);
        localization_is_not_working(translation);
    }
}
