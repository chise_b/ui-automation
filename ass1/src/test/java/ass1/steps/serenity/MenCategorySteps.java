package ass1.steps.serenity;

import ass1.pages.LandingPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MenCategorySteps {

    LandingPage landingPage;
    private static final String MEN = "MEN";

    @Step
    public void select_men_menu() {
        landingPage.lookup_men_menu();
    }

    @Step
    public void is_the_home_page() {
        landingPage.open();
    }

    @Step
    public void check_is_on_men_page(){

        assertThat(landingPage.getPageTitle().getText(), equalTo(MEN));
        assertThat(landingPage.getPathText().getText(), equalTo(MEN));
    }
}
