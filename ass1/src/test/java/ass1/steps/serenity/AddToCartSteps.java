package ass1.steps.serenity;

import ass1.pages.LandingPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AddToCartSteps {

    LandingPage landingPage;
    private static final String SUCCESSFULLY_ADDED_TO_CART_MESSAGE = "Linen Blazer was added to your shopping cart.";

    @Step
    public void is_the_home_page() {
        landingPage.open();
    }

    @Step
    public void select_first_new_product() {
        landingPage.lookup_first_new_product();
    }

    @Step
    public void select_color() {
        landingPage.select_color();
    }

    @Step
    public void select_size() {
        landingPage.select_size();
    }

    @Step
    public void add_to_cart() {
        landingPage.click_add_to_cart_button();
    }

    @Step
    public void check_product_added_successfully_to_cart() {
        assertThat(SUCCESSFULLY_ADDED_TO_CART_MESSAGE, equalTo(landingPage.getSuccessfullyAddedToCartMessage()));
    }
}
