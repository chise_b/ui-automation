package ass1.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://qa1.dev.evozon.com/")
public class LandingPage extends PageObject {

    @FindBy(xpath = "/html/body/div/div/header/div/div[3]/nav/ol/li[2]")
    private WebElementFacade menMenu;

    @FindBy(xpath = "/html/body/div/div/header/div/div[3]/nav/ol/li[2]/a")
    private WebElementFacade newArrivalsMenu;

    @FindBy(className = "page-title")
    private WebElementFacade pageTitle;

    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div[1]/ul/li[2]/strong")
    private WebElementFacade pathText;

    @FindBy(id = "select-language")
    private WebElementFacade languageSelect;

    @FindBy(id = "search")
    private WebElementFacade searchBox;

    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div/div[2]/div[2]/div[2]/ul/li[1]/div/h3/a")
    private WebElementFacade firstNewProduct;

    @FindBy(xpath = "//*[@id=\"product_addtocart_form\"]/div[3]/div[6]/div[2]/div[2]/button")
    private WebElementFacade addToCartButton;

    @FindBy(xpath = "//*[@id=\"swatch22\"]/span[1]/img")
    private WebElementFacade color;

    @FindBy(xpath = "//*[@id=\"swatch81\"]/span[1]")
    private WebElementFacade size;

    @FindBy(xpath = "//*[@id=\"top\"]/body/div/div/div[2]/div/div/div[2]/ul/li/ul/li/span")
    private WebElementFacade successfullyAddedToCartMessage;

    public void lookup_men_menu() {
        menMenu.click();
    }

    public WebElementFacade getPathText() {
        return pathText;
    }

    public WebElementFacade getPageTitle() {
        return pageTitle;
    }

    public void change_language(int index) {
        languageSelect.selectByIndex(index);
    }

    public String getSearchBoxPlaceHolder() {
        return searchBox.getAttribute("placeholder");
    }

    public void lookup_first_new_product() {
        firstNewProduct.click();
    }

    public void select_color() {
        color.click();
    }

    public void select_size() {
        size.click();
    }

    public void click_add_to_cart_button() {
        addToCartButton.click();
    }

    public String getSuccessfullyAddedToCartMessage() {
        return successfullyAddedToCartMessage.getText();
    }
}
